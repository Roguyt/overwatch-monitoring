# Overwatch Monitoring

Monitoring bot for website / ips/ ports.
Notification with Telegram / Slack.
Currently running on a Nodejs application with Redis.

## Features:

* Check if ip is up or down (with retry attempt)
* Check if website is up or down (soon verification of response)
* Request time / Down time measured
* Notification via Telegram / Slack
* Command to control the bot directly through Telegram / Slack:
  * !status get a report of all your monitoring

## Information

TODO: Later

## Installation
1. Clone this repository
2. Use yarn or npm to install the dependencies
3. Install redis-server
4. Run yarn run start / npm run start
5. Enjoy
6. (Optional) Add ecosystem.config.js to your PM2 System.

## Configuration
1. Copy src/configs/config.sample.js to src/configs/config.js
2. Adjust the settings
3. Restart