/**
 * Modules dependencies
 */

const HostController = require('../controllers').Host;

/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Update Servers Statuses Task
 */

HostController.updateHostsStatus();

setInterval(function () {
    HostController
        .updateHostsStatus()
    ;
}, config.schedule.delay);

