/**
 * Modules dependencies
 */

const fs        = require('fs');
const path      = require('path');

/**
 * Additional dependencies
 */

const basename    = path.basename(__filename);
const controllers = {};

/**
 * Model bootstrap
 */

fs
    .readdirSync(__dirname)
    .filter(file =>
        (file.indexOf('.') !== 0) &&
        (file !== basename) &&
        (file !== 'telegram.js' && file !== 'slack.js') &&
        (file.slice(-3) === '.js'))
    .forEach(file => {
        const controller = require(path.join(__dirname, file));
        file = file.replace('.js', '');
        controllers[file[0].toUpperCase() + file.substring(1)] = controller;
    });

module.exports = controllers;
