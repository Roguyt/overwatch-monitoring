/**
 * Modules dependencies
 */

const TemplateController = require('./template');
const CacheController = require('./cache');

/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Command controller
 */

const self = module.exports = (interface) => {
    let interfaceConfig = interface.getConfig();

    interface
        .match('!help', (err, channelId) => {
            TemplateController
                .render('help', {
                    channelId: channelId,
                    version: config.version
                })
                .then((data) => {
                    interface.send(data.message, data.args.channelId);
                })
                .catch((err) => {
                    console.log(err);
                })
            ;
        })
    ;

    interface
        .match('!status', (err) => {
            CacheController
                .get()
                .then((data) => {
                    return TemplateController
                        .render('status', {
                            date: data.date,
                            status: data.datas
                        })
                })
                .then((data) => {
                    interface.send(data, interfaceConfig.channel);
                })
                .catch((err) => {
                    console.log(err);
                })
            ;
        })
    ;
};