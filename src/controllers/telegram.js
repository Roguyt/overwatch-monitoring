/**
 * Modules dependencies
 */

const telegramBot = require('node-telegram-bot-api');

/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Initialize Telegram Client
 */

let client = new telegramBot(config.telegram.token, {
    polling: true
});

/**
 * Command controller
 */

const self = module.exports = {
    getConfig: () => {
        return config.telegram;
    },
    match: (word, callback) => {
        client.onText(new RegExp(word), (message, match) => {
            if (word === '!help') {
                callback(false, message.chat.id)
            } else {
                callback(false)
            }
        });
    },
    send: (message, channelId) => {
        if (!channelId) {
            channelId = config.telegram.channel;
        }

        client.sendMessage(channelId, message);
    }
};