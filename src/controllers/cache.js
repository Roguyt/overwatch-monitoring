/**
 * Modules dependencies
 */

const redis = require('redis');
const bluebird = require('bluebird');

/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Initialize Redis Client
 */

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const redisClient = redis.createClient();
const pub = redis.createClient();

/**
 * Cache controller
 */

const self = module.exports = {
    get: () => {
        return new Promise((resolve, reject) => {
            redisClient
                .getAsync(config.redis.name + ':data')
                .then((data) => {
                    if (data !== null && data.length > 0) {
                        data = JSON.parse(data);
                        resolve(data);
                    } else {
                        reject(); // TODO: CALL A ERROR
                    }
                })
            ;
        });
    },
    set: (datas) => {
        return new Promise((resolve, reject) => {
            datas = {
                date: new Date(),
                datas: datas
            };

            datas = JSON.stringify(datas);
            redisClient
                .set(config.redis.name + ':data', datas, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        pub.publish(config.redis.name + ':data:notify', "update");
                        resolve();
                    }
                })
            ;
        });
    }
};